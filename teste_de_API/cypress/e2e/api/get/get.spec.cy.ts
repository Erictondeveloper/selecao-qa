/// <reference types="cypress" />

describe('GET /bookingIds', function () {
	let uniqueID

	it('the bookings that exist within the API', function () {
		cy.getBookingIds().then((response) => {
			expect(response.status).to.eq(200)

			// Pegando um uniqueID
			let listLength = response.body.length
			let randomIndex = Math.floor(Math.random() * listLength)
			let randomBooking = response.body[randomIndex]
			uniqueID = randomBooking.bookingid
			cy.log('Random ID:', uniqueID)
		})
	})
	it('booking based upon the booking id provided', function () {
		expect(uniqueID).to.not.be.undefined
		cy.getIDbooking(uniqueID).then((response) => {
			expect(response.status).to.eq(200)
		})
	})
})
