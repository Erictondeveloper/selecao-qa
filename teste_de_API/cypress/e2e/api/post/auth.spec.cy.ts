/// <reference types="cypress" />

describe('POST/', function () {
	it('Generating a Token', function () {
		cy.generatingToken().then((response) => {
			expect(response.status).to.eq(200)
			let token = response.body.token
			cy.log(token)
		})
	})
})
