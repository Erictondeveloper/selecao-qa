export let validJSON = {
	firstname: 'Jim',
	lastname: 'Brown',
	totalprice: 111,
	depositpaid: true,
	bookingdates: {
		checkin: '2018-01-01',
		checkout: '2019-01-01',
	},
	additionalneeds: 'Breakfast',
}

export let userJSON = {
	username: 'admin',
	password: 'password123',
}
