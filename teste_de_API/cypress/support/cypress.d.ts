declare namespace Cypress {
    interface Chainable<Subject> {
        /**
         * Obtém os IDs de reserva de um recurso específico. Útil para testes que necessitam verificar ou manipular múltiplas reservas.
         * @returns {Chainable<Array<string>>} Uma lista contendo os IDs de reserva.
         */
        getLineALL(): Chainable<Array<string>>;

        /**
         * Obtém o ID de uma reserva específica, geralmente a mais recente ou aquela que atende a determinados critérios.
         * @returns {Chainable<string>} O ID de uma reserva específica.
         */
        getIDbooking(): Chainable<string>;

        /**
         * Gera um token de autenticação para ser usado em requisições de API, assegurando que os testes possam interagir com endpoints autenticados.
         * @returns {Chainable<string>} Um token de autenticação.
         */
        generatingToken(): Chainable<string>;

        /**
         * Executa uma requisição POST customizada com o corpo e URL especificados, facilitando a interação com APIs que requerem dados específicos.
         * @param url {string} A URL da API para a qual a requisição será enviada.
         * @param requestBody {object} O corpo da requisição a ser enviado.
         * @returns {Chainable<Response>} A resposta da requisição POST.
         */
        customPostRequest(url: string, requestBody: object): Chainable<Response>;
    }
}
