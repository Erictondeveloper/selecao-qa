/// <reference types="cypress" />

import { validJSON, userJSON } from '../fixtures/body/bodyValid'

Cypress.Commands.add('CreateBoonking', function () {
	cy.api({
		method: 'POST',
		url: 'booking',
		body: validJSON,
		failOnStatusCode: false,
	}).then((response) => {
		return response
	})
})

Cypress.Commands.add('generatingToken', function () {
	cy.api({
		method: 'POST',
		url: 'auth',
		body: userJSON,
		failOnStatusCode: false,
	}).then((response) => {
		return response
	})
})

Cypress.Commands.add('getBookingIds', function () {
	cy.api({
		method: 'GET',
		url: 'booking',
		failOnStatusCode: true,
	}).then((response) => {
		return response
	})
})

Cypress.Commands.add('getIDbooking', function (uniqueID) {
	expect(uniqueID).to.not.be.undefined
	cy.api({
		method: 'GET',
		url: `booking/${uniqueID}`,
		failOnStatusCode: false,
	}).then((response) => {
		return response
	})
})
