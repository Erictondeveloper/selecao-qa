import { defineConfig } from 'cypress'

export default defineConfig({
	e2e: {
		baseUrl: 'https://restful-booker.herokuapp.com/',
		setupNodeEvents(on, config) {
			// implement node event listeners here
		},
	},
	video: false,
	defaultCommandTimeout: 10000,
	env: {
		CYPRESS_PROJECT_TESTE_API: 'teste_de_API',
	},
})
