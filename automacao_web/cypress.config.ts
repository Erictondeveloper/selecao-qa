import { defineConfig } from 'cypress'

export default defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'https://buscacepinter.correios.com.br/app/endereco/index.php?t',
  },
  video: false,
	defaultCommandTimeout: 10000,
  env: {
    'CYPRESS_PROJECT_AUTOMACAO_WEB': 'automacao_web',
    "baseUrl_2": "https://demo.nopcommerce.com/electronics"
  }
})