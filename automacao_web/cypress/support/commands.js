/* eslint-disable cypress/no-unnecessary-waiting */
Cypress.Commands.add('setupTest', () => {
  // Configurações iniciais antes de cada teste
  cy.viewport(1920, 1080)
  // Acesse o site BuscaCEP Correios
  cy.visit('/')
  cy.wait(3000)
})

Cypress.Commands.add('interceptPostCEP', () => {
  cy.intercept('POST', '/app/endereco/carrega-cep-endereco.php').as('postCEP')
})

Cypress.Commands.add('waitForPostCEPInterception', () => {
  cy.wait('@postCEP').then((interception) => {
    expect(interception.response.statusCode).to.eq(200)
  })
})

Cypress.Commands.add('verifyAlertMessage', (expectedText) => {
  cy.get('#mensagem-resultado-alerta h6')
    .should('be.visible')
    .should('have.text', expectedText)
})

Cypress.Commands.add('verifyErrorMessage', (expectedText) => {
  cy.get('#alerta div')
    .should('be.visible')
    .should('have.text', expectedText)
})

Cypress.Commands.add('verifyElement', (selector, expectedText) => {
  cy.get(selector)
    .should('be.visible')
    .should('have.text', expectedText)
})





