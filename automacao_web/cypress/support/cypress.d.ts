declare namespace Cypress {
    interface Chainable<Subject> {
        /**
         * Configura o teste com operações iniciais padrão.
         * @example cy.setupTest()
         */
        setupTest(): Chainable<void>;

        /**
         * Intercepta requisições POST para uma determinada URL de CEP.
         * Pode ser utilizado para mocar a resposta de uma API de CEP.
         * @example cy.interceptPostCEP()
         */
        interceptPostCEP(): Chainable<void>;

        /**
         * Aguarda até que uma interceptação de POST para CEP seja concluída.
         * Útil para assegurar que a resposta foi recebida antes de prosseguir.
         * @example cy.waitForPostCEPInterception()
         */
        waitForPostCEPInterception(): Chainable<void>;

        /**
         * Verifica a mensagem de alerta na interface do usuário.
         * Deve ser ajustado para incluir parâmetros caso necessário.
         * @example cy.verifyAlertMessage('Mensagem esperada')
         */
        verifyAlertMessage(expectedMessage: string): Chainable<void>;

        /**
         * Verifica a mensagem de erro na interface do usuário.
         * Deve ser ajustado para incluir parâmetros caso necessário.
         * @example cy.verifyErrorMessage('Mensagem de erro esperada')
         */
        verifyErrorMessage(expectedMessage: string): Chainable<void>;
    }
}
