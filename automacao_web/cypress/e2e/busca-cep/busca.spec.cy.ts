/* eslint-disable cypress/no-unnecessary-waiting */
/// <reference types="cypress" />
import { cepValido } from '../../fixtures/ceps/cepValidos'
import { cepInvalido } from '../../fixtures/ceps/cepInvalidos'

// TESTE CONFORMIDADE
describe('Busca por CEP e NOME Válidos', () => {
  // Gancho
  beforeEach(() => {
    cy.setupTest()
  })

  // Cenário 1
  it('Busca por um CEP existente', () => {
    cy.get('#endereco').should('be.visible').type(cepValido.cepExistente)
    cy.wait(4000)

    cy.interceptPostCEP()
    cy.get('#btn_pesquisar').click()

    cy.waitForPostCEPInterception()
  })

  // Cenário 2
  it('Busca por um CEP com formato alternativo', () => {
    cy.get('#endereco').should('be.visible').type(cepValido.cepFormatoAlternado)
    cy.wait(4000)

    cy.interceptPostCEP()
    cy.get('#btn_pesquisar').click()

    cy.waitForPostCEPInterception()
  })

  // Cenário 3
  it('Busca por um por CEP sem caracteres especiais', () => {
    cy.get('#endereco').should('be.visible').type(cepValido.cepCaracteresEspeciais)
    cy.wait(4000)

    cy.interceptPostCEP()
    cy.get('#btn_pesquisar').click()

    cy.waitForPostCEPInterception()
  })

   // Cenário 4
   it('Busca por um por CEP com espaços em branco no lugar do hífen', () => {
    cy.get('#endereco').should('be.visible').type(cepValido.cepEspaçosBranco)
    cy.wait(4000)

    cy.interceptPostCEP()
    cy.get('#btn_pesquisar').click()

    cy.waitForPostCEPInterception()
  })

  // Cenário 5
  it('Busca por um nome completo', () => {
    cy.get('#endereco').should('be.visible').type(cepValido.nomeCompleto)
    cy.wait(4000)

    cy.interceptPostCEP()
    cy.get('#btn_pesquisar').click()

    cy.waitForPostCEPInterception()
  })

  // Cenário 6
  it('Busca Pelo segundo nome', () => {
    cy.get('#endereco').should('be.visible').type(cepValido.segundoNome)
    cy.wait(4000)

    cy.interceptPostCEP()
    cy.get('#btn_pesquisar').click()

    cy.waitForPostCEPInterception()
  })
})

// TESTE DE ROBUSTEZ
describe('Busca por CEP e NOME Inválidos', () => {
  // Gancho
  beforeEach(() => {
    cy.setupTest()
  })

  // Cenário 7
  it('Fazer uma busca por um CEP inexistente', () => {

    cy.get('#endereco').should('be.visible').type(cepInvalido.cepInexistente)
    cy.wait(4000)

    cy.interceptPostCEP()

    cy.get('#btn_pesquisar').click()
    cy.wait(2000)

    cy.waitForPostCEPInterception()

    cy.verifyAlertMessage('Dados não encontrado')
  })

  // Cenário 8
  it('Fazer uma busca por um CEP do formato inválido', () => {
    cy.get('#endereco').should('be.visible').type(cepInvalido.invalido)
    cy.wait(4000)

    cy.interceptPostCEP()

    cy.get('#btn_pesquisar').click()
    cy.wait(2000)

    cy.waitForPostCEPInterception()

    cy.verifyAlertMessage('Dados não encontrado')
  })

  // Cenário 9
  it('Fazer uma busca por um cep usando caracteres especiais inválidos', () => {
    cy.get('#endereco').should('be.visible').type(cepInvalido.cepCaracterEspcialInvalido)
    cy.wait(4000)

    cy.interceptPostCEP()


    cy.get('#btn_pesquisar').click()
    cy.wait(2000)

    cy.waitForPostCEPInterception()

    cy.verifyAlertMessage('Dados não encontrado')
  })

  // Cenário 10
  it('Fazer uma busca por um cep usando comprimento insuficiente', () => {
    cy.get('#endereco').should('be.visible').type(cepInvalido.cepComprimentoInsuficiente)
    cy.wait(4000)

    cy.get('#btn_pesquisar').click()

    cy.wait(2000)
    cy.verifyErrorMessage('Informe o Endereço com no mínimo 2(dois) caracteres!')
  })

  // Cenário 11
  it('Fazer uma busca com o campo vazio', () => {

    cy.get('#btn_pesquisar').click()
    cy.wait(4000)
    cy.verifyErrorMessage('Informe o Endereço com no mínimo 2(dois) caracteres!')
  })
})