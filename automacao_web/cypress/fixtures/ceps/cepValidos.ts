export let cepValido = {
    cepExistente: '69082-640',
    cepFormatoAlternado: '69082640',
    cepCaracteresEspeciais: '69.082640',
    cepEspaçosBranco: '69082 640',
    nomeCompleto: 'Instituto Creathus',
    segundoNome: 'Creathus'
}