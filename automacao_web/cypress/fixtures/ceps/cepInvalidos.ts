export let cepInvalido = {
    cepInexistente: '00000-000',
    invalido: '6907-5305',
    cepCaracterEspcialInvalido: '69@75-305',
    cepComprimentoInsuficiente: '1'
}