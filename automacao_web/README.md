## Configuração do Ambiente

## Precisa do [Node](https://nodejs.org/en/download/current) ou [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable) instalado

## Baixar as depedenências do projeto

    $ npm install

## Instalar o [Cypress](https://docs.cypress.io/guides/getting-started/installing-cypress)

    $ npm install cypress --save-dev

# ou

    $ yarn install

## Comando para roda o cypress

    $ npx cypress open

# ou

    $ yarn cypress open


