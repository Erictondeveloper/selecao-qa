# AUOTMAÇÂO DE TESTE CYPRESS-QA

<h1 align="center">Tecnologia Utilizada</h1>

# [Cypress Version: 13.6.3](https://docs.cypress.io/guides/getting-started/installing-cypress)
# [Node Version: v18.13.0](uhttps://nodejs.org/en/download/currentrl)
# NPM Version: v8.19.3
# [Yarn Version: v1.22.19](https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable)

## Projeto

## Automação Web

# O projeto de automação web utiliza o Cypress para realizar testes end-to-end na aplicação Marvelopédia.

# Estrutura do Projeto
Estrutura das pastas
=================
<!--ts-->
* [automação_Web]
   * [cenarios teste]
      * [Caderno de teste em PDF]
      * [Caderno de teste em TXT]
   * [[cypress]](https://www.cypress.io/)
      * [e2e]
         * [busca Cep]
            * [busca.spec.cy.js]
      * [fixtures]
         * [cep válido]
         * [cep inválido]
      * [support]
         * [Commands]
         * [e2e]
   * [Cypress.config.js]
   * [package-lock]
   * [package.json]
* [Marvelopedia]
   * [Caderno de teste]
      * [Caderno dos Casos de Teste PDF]
   * [Parecer técnico]
      * [Parecer Técnico - Marvelopédia em PDF]
   * [Plano de Teste]
      * [Plano de Teste Marvelopédia em PDF]
   * [Relatório de bugs]
      * [Relatórios de Bugs.pdf]
   * [Relatório de Execução Marvelopédia]
      * [Grafico 1]
      * [Grafico 2]
      * [Relatório de Execução Marvelopédia em PDF]
* [Teste de API]
   * [Caderno de teste]
      * [Caderno dos Casos de Teste API PDF]
   * [Relatório de Execução API]
      * [Relatório de Execução API em PDF]
   * [Evidencias encontradas]
      * [Delete]
      * [Get ID]
      * [Patch]
      * [Post]
      * [Post Auth] 
      * [Put]    
<!--te-->

